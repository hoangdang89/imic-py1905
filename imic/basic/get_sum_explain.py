
danhSach = [1, 2, 5, 30, 100]

tong = 0                    #khoi tao            -> tong = 0
tong = tong + danhSach[0]   #tong = tong + 1     -> tong = 1
tong = tong + danhSach[1]   #tong = tong + 2     -> tong = 3
tong = tong + danhSach[2]   #tong = tong + 5     -> tong = 8
tong = tong + danhSach[3]   #tong = tong + 30    -> tong = 38
tong = tong + danhSach[4]   #tong = tong + 100   -> tong = 138
#for number in danhSach:
#    tong = tong + number   #Loop 5 lan nhu vi du o tren.

print("tong = {}".format(tong))
